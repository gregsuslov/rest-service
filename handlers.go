package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func getUsers(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(selectAll())
}

func getUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.ParseUint(params["id"], 0, 0)
	json.NewEncoder(w).Encode(selectUser(int(id)))
}

func addUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	json.NewEncoder(w).Encode(insertUser(params["name"]))
}

func delUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.ParseUint(params["id"], 0, 0)
	res := deleteUser(int(id))
	var user User
	if res {
		user = User{ID: int(id), Name: "deleted"}
	}
	json.NewEncoder(w).Encode(user)
}

func updUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.ParseUint(params["id"], 0, 0)
	user := User{ID: int(id), Name: params["name"]}
	res := updateUser(user)
	if !res {
		user = User{ID: 0, Name: ""}
	}
	json.NewEncoder(w).Encode(user)
}
